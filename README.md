# Wonderlic

## Required technologies

* [Python] - Python in version >= 3.7.4
* [SQLite3] - Database
#
## Python packages manager
* [Poetry] - Dependency manager tracking local dependencies of your projects and libraries.
* [Pip] - Simple python packager manager
#
## Django version and Libraries

* [Django 3.0.6] - Python library for web apps
* [Django Rest Framework 3.11.0] - Django library for web api's
* [Django Rest Auth 0.9.5] - Django library for generate tokens
* [Django Filter 2.4.0] - Django library for filtering
#

## Clone project

```sh
$ git clone https://luigus@bitbucket.org/luigus/wonderlic.git
```

#
## Setup project
#### You can setup the project using virtualenv or poetry.

#### Using pip and virtualenv
```sh
$ cd wonderlic/
$ virtualenv .venv -p python3.7
$ source .venv/bin/activate
$ pip install -r requirements.txt
```

#### Using Poetry
```sh
$ cd wonderlic/
$ mkdir .venv
$ poetry env use 3.7
$ poetry shell
$ poetry install
```
#
## Setup database
#### Setup the database with the registration of companies and reviewers
```sh
$ cd wonderlic/web
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py initial_data
```
#### After the script has finished running, you will have access to users and access tokens to use the api.
#
### List of reviewers and their respective companies
| REVIEWER 			   | COMPANY 	    | PERMISSION 	
| ------ 			   | ------ 	    |------ 			
| admin                | - 		        | API and Admin site
| jose 	               | Google 	    | API      
| jesus 	           | Bairesdev 		| API
| joao 	               | Facebook 	    | API
| thiago               | - 	            | API

 
#
## Run Django Web App
#### You need to have the environment enabled
```sh
$ cd wonderlic/web
$ python manage.py runserver 0.0.0.0:8000
```
#### The following list of links will be available after the project starts
* http://localhost:8000/admin
* http://localhost:8000/api 
* http://localhost:8000/api-token-auth/ 

#
## Admin site 
* Link: http://localhost:8000/admin
#### To access the administration site, it is necessary to enter the username and password.
* Username: admin
* Password: admin
#
## API
| API 				   | METHOD 	| ENDPOINTS 		  | USER 			 
| ------ 			   | ------ 	|------ 			  |------ 		
| List of companies    | GET 		| /api/company/ 	  | Any      
| Company              | GET 		| /api/company/:id 	  | Any      
| New company          | POST 		| /api/company/       | Admin      
| List of reviewers    | GET 		| /api/reviewer/ 	  | Any      
| Reviewers            | GET 		| /api/reviewer/:id   | Any      
| New reviewer         | POST 		| /api/reviewer/      | Admin      
| List of reviews      | GET 		| /api/review/ 	      | Owner/Admin      
| Review               | GET 		| /api/review/:id     | Owner/Admin   
| New review           | POST 		| /api/review/        | Owner/Admin
| Update review        | PUT 		| /api/review/:id     | Owner/Admin

#
## Get user token
* Link: http://localhost:8000/api-token-auth/
#### It is possible to retrieve the token if the user has forgotten using the link above by sending username and password via post
#
## Tests
#### Models tests and api tests have been developed for this application
#### To run tests of models
```sh
$ cd wonderlic/web
$ python manage.py test reviews.tests.test_models
```

#### To run API tests
```sh
$ cd wonderlic/web
$ python manage.py test reviews.tests.test_api
```
   

