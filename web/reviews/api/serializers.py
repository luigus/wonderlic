from django.contrib.auth.models import User
from rest_framework import serializers
from reviews.models import Review, Reviewer, Company


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'


class ReviewerSerializer(serializers.ModelSerializer):

    user = serializers.CharField(source="user.username")
    email = serializers.CharField(source="user.email")
    password = serializers.CharField(source="user.password")

    class Meta:
        model = Reviewer
        fields = ['id', 'user', 'email', 'company', 'password']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['company'] = CompanySerializer(instance.company).data
        return response

    def create(self, validated_data):

        user_name = validated_data['user']['username']
        user_email = validated_data['user']['email']
        user_password = validated_data['user']['password']

        if len(User.objects.filter(username=user_name)) == 0:
            user = User()
            user.username = user_name
            user.email = user_email
            user.set_password(user_password)
            user.is_superuser = False
            user.is_active = True
            user.is_staff = False
            user.save()
        else:
            user = User.objects.get(username=user_name)

        company = None
        if "company" in validated_data and len(Company.objects.filter(name=validated_data['company'])) > 0:
            company = Company.objects.get(name=validated_data['company'])

        return Reviewer.objects.create(user=user, company=company)


class ReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Review
        fields = '__all__'
        read_only_fields = ['ip_address']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['reviewer'] = ReviewerSerializer(instance.reviewer).data
        return response

    def create(self, validated_data):
        validated_data['ip_address'] = self.context.get('ip')
        return Review.objects.create(**validated_data)
