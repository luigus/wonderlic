
from django.urls import path, include
from .viewsets import CompanyViewset, ReviewerViewset, ReviewViewset
from rest_framework import routers

routers = routers.DefaultRouter()
routers.register('company', CompanyViewset)
routers.register('reviewer', ReviewerViewset)
routers.register('review', ReviewViewset)

urlpatterns = [
    path('', include(routers.urls)),
]
