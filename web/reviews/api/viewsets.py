
from django.contrib.auth.models import User
from rest_framework import viewsets, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.response import Response

from reviews.models import Review, Company, Reviewer
from .serializers import ReviewerSerializer, ReviewSerializer, CompanySerializer


class CompanyViewset(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly, )
    authentication_classes = [TokenAuthentication]
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def create(self, request, *args, **kwargs):

        user = request.user
        if user.is_superuser:

            if "name" in request.data:
                company_name = request.data['name']

                find_name = False
                for company in Company.objects.all():
                    if company_name.lower() == company.name.lower():
                        find_name = True
                        break

                if find_name:
                    return Response({'error': 'Company name already exists.'}, status=status.HTTP_400_BAD_REQUEST)

            serializer = CompanySerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({'error': 'Only Admin is allowed to create new Company.'},
                            status=status.HTTP_401_UNAUTHORIZED)


class ReviewerViewset(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    authentication_classes = [TokenAuthentication]
    queryset = Reviewer.objects.all()
    serializer_class = ReviewerSerializer

    def create(self, request, *args, **kwargs):

        user = request.user
        if user.is_superuser:

            if "company" in request.data:
                company = request.data['company']
                if isinstance(company, str):
                    if len(Company.objects.filter(name=company)) > 0:
                        company_obj = Company.objects.get(name=company)
                        request.data['company'] = company_obj.pk
                    else:
                        return Response({'error': 'Invalid company name.'}, status=status.HTTP_400_BAD_REQUEST)

            serializer = ReviewerSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({'error': 'Only Admin is allowed to create reviewers.'},
                            status=status.HTTP_401_UNAUTHORIZED)


class ReviewViewset(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = [TokenAuthentication]
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

    def get_queryset(self):
        user = self.request.user
        if user.id is not None:
            user = User.objects.get(pk=user.id)
            if user.is_superuser:
                return Review.objects.all()
            else:
                rev = Reviewer.objects.get(user=user)
                return Review.objects.filter(reviewer=rev)

    def get_client_ip(self):
        ip = self.request.META.get('HTTP_CF_CONNECTING_IP')
        if ip is None:
            ip = self.request.META.get('REMOTE_ADDR')
        return ip

    def create(self, request, *args, **kwargs):

        same_user = False
        user = request.user

        if user.id is not None:
            user = User.objects.get(pk=user.id)
            reviewer = request.data['reviewer']

            if isinstance(reviewer, str):
                request.data['reviewer'] = user.id
                if user.username == reviewer:
                    same_user = True

            if isinstance(reviewer, int) and user.id == reviewer:
                same_user = True

            if user.is_superuser or same_user:
                serializer = ReviewSerializer(data=request.data, context={'ip': str(self.get_client_ip())})
                serializer.is_valid(raise_exception=True)
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response({'error': 'It is not allowed to create reviews for other users'},
                                status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)

    def update(self, request, *args, **kwargs):
        same_user = False
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        user = request.user

        if user.id is not None:
            user = User.objects.get(pk=user.id)
            reviewer = request.data['reviewer']

            if isinstance(reviewer, str):
                request.data['reviewer'] = user.id
                if user.username == reviewer:
                    same_user = True

            if isinstance(reviewer, int) and reviewer == user.id:
                same_user = True

            if user.is_superuser or same_user:
                serializer = self.get_serializer(instance, data=request.data, partial=partial)
                serializer.is_valid(raise_exception=True)
                self.perform_update(serializer)
                return Response(serializer.data)
            else:
                return Response({'error': 'It is not allowed update reviews for other users'},
                                status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)
