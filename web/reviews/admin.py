
from django.contrib import admin
from .models import Reviewer, Review, Company


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('title', 'reviewer', 'submission')

    class Meta:
        model = Review


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('title', 'reviewer', 'submission')

    class Meta:
        model = Review


admin.site.register(Company)
admin.site.register(Reviewer)
admin.site.register(Review, ReviewAdmin)
