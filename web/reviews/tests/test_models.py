from django.test import TestCase
from reviews.models import Company, Reviewer, Review
from django.contrib.auth import get_user_model


# - - - - - - - - - - - - - - - - - - - - - - -
# To run tests:
# python manage.py test reviews.tests.test_models
# - - - - - - - - - - - - - - - - - - - - - - -

class CompanyTestCase(TestCase):

    def setUp(self):
        self.name = "Facebook"
        Company.objects.create(name=self.name)

    def test_find_company(self):
        """Test with we can find a Company by name"""
        facebook = Company.objects.get(name=self.name)
        self.assertEqual(facebook.name, self.name)

    def test_company_id(self):
        """Test with we can find a Company by ID"""
        facebook = Company.objects.get(pk=1)
        self.assertEqual(facebook.name, self.name)

    def test_company_wrong_id(self):
        """Test with we cannot find a Company by ID"""
        facebook = Company.objects.filter(pk=199)
        self.assertEqual(len(facebook), 0)


class ReviewerTestCase(TestCase):

    def setUp(self):
        self.company_name = "Test1"
        self.company = Company.objects.create(name=self.company_name)
        self.user_name = 'test'
        self.user_email = 'test@teste.com'
        self.user_password = "viva2021!"

        user_obj = get_user_model()
        user = user_obj.objects.create_user(username=self.user_name, email=self.user_email, password=self.user_password)
        user.is_superuser = False
        user.is_staff = False
        user.save()

        self.user = user
        self.reviewer = Reviewer.objects.create(user=self.user, company=self.company)

    def test_find_reviewer_by_name(self):
        """Test with we can find a Reviewer by name"""
        reviewer = Reviewer.objects.get(company__name=self.company_name, user=self.user)
        self.assertEqual(reviewer.user.username, self.user_name)

    def test_find_reviewer_by_company(self):
        """Test with we can find a Reviewer by email"""
        reviewer = Reviewer.objects.get(company__name=self.company_name, user=self.user)
        self.assertEqual(reviewer.company.name, self.company_name)

    def test_find_wrong_reviewer(self):
        """Test with we cannot find a Reviewer by company name"""
        reviewer = Reviewer.objects.filter(company__name='')
        self.assertEqual(len(reviewer), 0)


class ReviewTestCase(TestCase):

    def setUp(self):
        self.company_name = "Test1"
        self.company = Company.objects.create(name=self.company_name)
        self.user_name = 'test'
        self.user_email = 'test@teste.com'
        self.user_password = "viva2021!"
        self.ip_address = "127.0.0.1"

        user_obj = get_user_model()
        user = user_obj.objects.create_user(username=self.user_name, email=self.user_email,
                                            password=self.user_password)
        user.is_superuser = False
        user.is_staff = False
        user.save()

        self.user = user
        self.reviewer = Reviewer.objects.create(user=self.user, company=self.company)

        self.title = "Title"
        self.summary = "Sumary 1 2 3 4 5"
        self.rating = 5
        self.reviewer_id = 1
        self.reviewer_name = self.user_name

    def test_find_review(self):
        """Test with we can find a Review by title, summary, rating and ip_address"""
        review = Review.objects.create(title=self.title,
                                            summary=self.summary,
                                            rating=self.rating,
                                            reviewer=self.reviewer,
                                            ip_address=self.ip_address)

        self.assertEqual(review.title, self.title)
        self.assertEqual(review.summary, self.summary)
        self.assertEqual(review.rating, self.rating)
        self.assertEqual(review.ip_address, self.ip_address)

    def test_not_find_review(self):
        """Test with we cannot find a Review by title, summary, rating and ip_address"""
        review = Review.objects.create(title=self.title,
                                            summary=self.summary,
                                            rating=self.rating,
                                            reviewer=self.reviewer,
                                            ip_address=self.ip_address)

        self.assertNotEqual(review.title, " ")
        self.assertNotEqual(review.summary, " ")
        self.assertNotEqual(review.rating, 6)
        self.assertNotEqual(review.ip_address, " ")

