from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User

from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status

import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "admin.settings")
django.setup()


# - - - - - - - - - - - - - - - - - - - - - - -
# To run tests:
# python manage.py test reviews.tests.test_api
# - - - - - - - - - - - - - - - - - - - - - - -


URL = 'http://127.0.0.1:8000/api/'
URL_COMPANY = URL + "company/"
URL_REVIEWER = URL + "reviewer/"
URL_REVIEW = URL + "review/"

TOKEN = "Token "
ADMIN_USER = "admin"
ADMIN_EMAIL = "admin123@email.com"
ADMIN_PASSWD = "admin"

data_company = {'name': 'Bairesdev'}
data_invalid_company = {'id': 123}

data_reviewer = {'user': 'jesus', 'email': 'jesus@bairesdev.com', 'company': 'Bairesdev'}
data_reviewer1 = {'user': 'user1', 'email': 'user1@user1.com', 'company': 'Bairesdev'}
data_reviewer2 = {'user': 'user2', 'email': 'user2@user2.com', 'company': 'Bairesdev'}
data_invalid_reviewer = {'user': ' ', 'email': ' ', 'company': ' '}

data_review = {"title": "Title", "rating": 3, "summary": "Summary", "reviewer": "jesus"}
data_review1 = {"title": "Title 1", "rating": 1, "summary": "Summary 1", "reviewer": "user1"}
data_review2 = {"title": "Title 2", "rating": 2, "summary": "Summary 2", "reviewer": "user2"}
data_invalid_review1 = {"title": "Title", "rating": 31, "summary": "Summary", "reviewer": "jesus"}


class CompanyEndpointTestCase(TestCase):

    def setUp(self):
        self.url = URL_COMPANY
        self.user_name = ADMIN_USER
        self.user_email = ADMIN_EMAIL
        self.user_password = ADMIN_PASSWD

        self.user = User.objects.create_superuser(self.user_name, self.user_email, self.user_password)
        self.token = Token.objects.create(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN + self.token.key)

    def test_valid_data(self):
        r = self.client.post(self.url, data=data_company, format='json')
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)
        data = r.json()

        self.assertIn('id', data)
        self.assertIsInstance(data['id'], int)
        self.assertEqual(data['name'], data_company['name'])

    def test_invalid_data(self):
        r = self.client.post(self.url, data=data_invalid_company, format='json')
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)

    def test_empty_data(self):
        r = self.client.post(self.url, data={}, format='json')
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_data(self):
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)


class ReviewerEndpointForAdminUserTestCase(TestCase):

    def setUp(self):
        self.url = URL
        self.user_name = ADMIN_USER
        self.user_email = ADMIN_EMAIL
        self.user_password = ADMIN_PASSWD

        self.user = User.objects.create_superuser(self.user_name, self.user_email, self.user_password)
        self.token = Token.objects.create(user=self.user)

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN + self.token.key)

    def test_get_valid_data(self):
        self.url = URL_REVIEWER
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)

    def test_post_valid_data(self):

        self.url = URL_COMPANY
        r = self.client.post(self.url, data=data_company, format='json')
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        self.url = URL_REVIEWER
        r = self.client.post(self.url, data=data_reviewer, format='json')
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        data = r.json()
        self.assertIn('id', data)
        self.assertIsInstance(data['id'], int)
        self.assertEqual(data['user'], data_reviewer['user'])
        self.assertEqual(data['email'], data_reviewer['email'])
        self.assertEqual(data['company']['name'], data_reviewer['company'])

    def test_post_invalid_data(self):
        self.url = URL_REVIEWER
        r = self.client.post(self.url, data=data_invalid_reviewer, format='json')
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_empty_data(self):
        self.url = URL_REVIEWER
        r = self.client.post(self.url, data={}, format='json')
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)


class ReviewerEndpointForInvalidUserTestCase(TestCase):

    def setUp(self):

        self.user = User()
        self.user.username = "test"
        self.user.email = "test@test.com"
        self.user.password = "viva2021!"
        self.user.is_superuser = False
        self.user.save()

        self.token = Token.objects.create(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN + self.token.key)

    def test_with_invalid_user(self):

        self.url = URL_COMPANY
        r = self.client.post(self.url, data=data_company, format='json')
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        self.url = URL_REVIEWER
        r = self.client.post(self.url, data=data_reviewer, format='json')
        self.assertEqual(r.status_code, status.HTTP_401_UNAUTHORIZED)


class ReviewEndpointTestCase(TestCase):

    def setUp(self):
        self.url = URL

        # Create user 1
        self.user1 = User()
        self.user1.username = "user1"
        self.user1.email = "user1@user1.com"
        self.user1.password = "viva2021!"
        self.user1.is_superuser = False
        self.user1.save()
        self.token1 = Token.objects.create(user=self.user1)

        # Create superuser
        self.superuser_name = ADMIN_USER
        self.superuser_email = ADMIN_EMAIL
        self.superuser_password = ADMIN_PASSWD
        self.superuser = User.objects.create_superuser(self.superuser_name,
                                                       self.superuser_email,
                                                       self.superuser_password)
        self.token_superuser = Token.objects.create(user=self.superuser)

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN + self.token_superuser.key)

    def test_post_review(self):
        self.url = URL_COMPANY
        r = self.client.post(self.url, data=data_company, format='json')
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        self.url = URL_REVIEWER
        r = self.client.post(self.url, data=data_reviewer1, format='json')
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        self.url = URL_REVIEW
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN + self.token1.key)
        r = self.client.post(self.url, data=data_review1, format='json')
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

    def test_get_review(self):

        self.url = URL_COMPANY
        r = self.client.post(self.url, data=data_company, format='json')
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        self.url = URL_REVIEWER
        r = self.client.post(self.url, data=data_reviewer1, format='json')
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        self.url = URL_REVIEW
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN + self.token1.key)
        r = self.client.post(self.url, data=data_review1, format='json')
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)


