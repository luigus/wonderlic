from django.db import models
from django.contrib.auth.models import User


class Company(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Reviewer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        if self.company is None:
            return f"{self.user.username}"
        else:
            return f"{self.user.username} [{self.company}]"


class Review(models.Model):

    RATING = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5)
    )

    title = models.CharField(max_length=64)
    rating = models.IntegerField(choices=RATING)
    summary = models.TextField()
    submission = models.DateField(auto_now=True)
    ip_address = models.GenericIPAddressField(protocol='ipv4',)
    reviewer = models.ForeignKey(Reviewer, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
